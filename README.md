# reservations-fastapi

This is a simple RESTful API built with Python and FastAPI for CRUD operations (Create, Read, Update, Delete) on users. FastAPI is a powerful web framework for building APIs.


## Installation

Clone this repository to your local machine:
```
git clone https://gitlab.com/sefi06/reservations-fastapi.git
```

Change into the project directory:
```
cd reservations-fastapi
```

Install the project dependencies:
```
pip install -r requirements.txt
```

Import reservations.sql file and Adjust the database connection in the db/database.php file:
```
self.conn = mysql.connector.connect(
            host="localhost",
            user="root",
            password="typepassword",
            database="reservations"
        )
```

Run the application:
```
uvicorn main:app --reload
```

The application will start and be available at http://localhost:8000.


## Documentation

The documentation can be seen at at http://localhost:8000/redoc