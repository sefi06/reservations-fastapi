def sequence_exists(numbers, sequence):
    if len(sequence) > len(numbers):
        return False

    for i in range(len(numbers) - len(sequence) + 1):
        if numbers[i:i + len(sequence)] == sequence:
            return True

    return False

main = [20, 7, 8, 10, 2, 5, 6]
seq = [7, 8]

print(sequence_exists(main, seq)) 

seq = [8, 7]

print(sequence_exists(main, seq))

seq = [7, 10]

print(sequence_exists(main, seq))