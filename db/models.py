from pydantic import BaseModel
from datetime import datetime, date
from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

class User(BaseModel):
    name: str
    email: str
    password: str

    def verify_password(self, plain_password: str) -> bool:
        return pwd_context.verify(plain_password, self.password)
    
class UserSchema(BaseModel):
    id: int
    name: str
    email: str
    password: str

class Reservation(BaseModel):
    id: int
    iduser: int
    date: date
    ticket_number: str

class ReservationCreate(BaseModel):
    date: date