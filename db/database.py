import mysql.connector
from db.models import User, Reservation, UserSchema
from datetime import datetime, date
from typing import List
from passlib.context import CryptContext
import sys

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

class Database:
    def __init__(self):
        self.conn = mysql.connector.connect(
            host="localhost",
            user="root",
            password="typepassword",
            database="reservations"
        )
        self.cursor = self.conn.cursor()

    async def create_user(self, user: User):
        hashed_password = pwd_context.hash(user.password) 
        query = "INSERT INTO users (name, email, password, created_at, updated_at) VALUES (%s, %s, %s, %s, %s)"
        self.cursor.execute(query, (user.name, user.email, hashed_password, datetime.now(), datetime.now()))
        self.conn.commit()

    async def get_user_by_email(self, email: str) -> UserSchema:
        query = "SELECT id, name, email, password FROM users WHERE email = %s"
        self.cursor.execute(query, (email,))
        result = self.cursor.fetchone()
        if result:
            return UserSchema(id=result[0], name=result[1], email=result[2], password=result[3])
        return None


    async def create_reservation(self, user_id: int, date: datetime, ticket_number: str):
        query = "INSERT INTO reservations (iduser, date, ticket_number, created_at, updated_at) VALUES (%s, %s, %s, %s, %s)"
        self.cursor.execute(query, (user_id, date, ticket_number, datetime.now(), datetime.now()))
        self.conn.commit()
        return self.cursor.lastrowid

    async def get_reservation_by_id(self, reservation_id: int) -> Reservation:
        query = "SELECT * FROM reservations WHERE id = %s"
        self.cursor.execute(query, (reservation_id,))
        result = self.cursor.fetchone()
        if result:
            return Reservation(id=result[0], iduser=result[1], date=result[2], ticket_number=str(result[3]))
        return None
    
    async def get_reservation_by_date(self, user_id: int, reservation_date: date) -> Reservation:
        query = "SELECT * FROM reservations WHERE iduser = %s AND date = %s"
        self.cursor.execute(query, (user_id, reservation_date))
        result = self.cursor.fetchone()
        if result:
            return Reservation(id=result[0], iduser=result[1], date=result[2], ticket_number=str(result[3]))
        return None

    async def get_reservations(self) -> List[Reservation]:
        query = "SELECT * FROM reservations"
        self.cursor.execute(query)
        results = self.cursor.fetchall()
        return [Reservation(id=result[0], iduser=result[1], date=result[2], ticket_number=result[3]) for result in results]

    async def update_reservation(self, reservation_id: int, user_id: int, ticket_number: int, date: datetime):
        query = "UPDATE reservations SET date = %s, ticket_number = %s, updated_at = %s WHERE id = %s AND iduser = %s"
        self.cursor.execute(query, (date, ticket_number, datetime.now(), reservation_id, user_id))
        self.conn.commit()
        return reservation_id

    async def delete_reservation(self, reservation_id: int, user_id: int):
        query = "DELETE FROM reservations WHERE id = %s AND iduser = %s"
        self.cursor.execute(query, (reservation_id, user_id))
        self.conn.commit()

    async def get_max_ticket_number(self, date: datetime.date):
        query = "SELECT MAX(ticket_number) FROM reservations WHERE date = %s"
        self.cursor.execute(query, (date,))
        result = self.cursor.fetchone()
        if result and result[0] is not None:
            return result[0]
        return 0

database = Database()
