from fastapi import FastAPI, HTTPException, Depends, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel
from datetime import datetime
from typing import List, Dict
from db.database import database
from db.models import User, Reservation, ReservationCreate
from datetime import datetime, timedelta
from jose import jwt, JWTError
import sys

SECRET_KEY = "your_secret_key"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 120

app = FastAPI()

# Token Security
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

# Authentication
async def authenticate_user(email: str, password: str):
    user = await database.get_user_by_email(email)
    if not user:
        return False
    if not user.verify_password(password):
        return False
    return user

async def authenticate_user_by_token(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        user_email = payload.get("sub")
        user = await database.get_user_by_email(user_email)

        if not user:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="User not found",
                headers={"WWW-Authenticate": "Bearer"},
            )
        return user
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

    
async def generate_ticket_number(date):
    max_ticket_number = await database.get_max_ticket_number(date)

    if not max_ticket_number:
        max_ticket_number = 0

    new_ticket_number = max_ticket_number + 1

    return new_ticket_number

def create_access_token(data: dict):
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    
    to_encode = {**data, "exp": expire}
    
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

# API Routes
@app.post("/login")
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=401, detail="Invalid credentials")
    access_token = create_access_token({"sub": user.email})
    return {"access_token": access_token, "token_type": "bearer"}

@app.post("/register")
async def register_user(user: User):
    existing_user = await database.get_user_by_email(user.email)
    if existing_user:
        raise HTTPException(status_code=400, detail="Email already registered.")
    await database.create_user(user)
    return {"message": "User created successfully"}

@app.get("/reservations/{reservation_id}")
async def get_reservations(reservation_id: int):
    return await database.get_reservation_by_id(reservation_id)

@app.post("/reservations")
async def create_reservation(reservation: ReservationCreate, token: str = Depends(oauth2_scheme)):
    user = await authenticate_user_by_token(token)
    if not user:
        raise HTTPException(status_code=401, detail="Invalid token")

    if reservation.date < datetime.now().date():
        raise HTTPException(status_code=400, detail="The date must be today or later.")
    
    existing_reservation = await database.get_reservation_by_date(user.id, reservation.date)
    if existing_reservation:
        raise HTTPException(status_code=400, detail="You have already created a reservation for this date.")

    ticket_number = await generate_ticket_number(reservation.date)
    reservation_id = await database.create_reservation(user.id, reservation.date, ticket_number)
    created_reservation = await database.get_reservation_by_id(reservation_id)
    return {"message": "Reservation created successfully", "data": created_reservation}

@app.put("/reservations/{reservation_id}")
async def update_reservation(reservation_id: int, reservation: ReservationCreate, token: str = Depends(oauth2_scheme)):
    user = await authenticate_user_by_token(token)
    if not user:
        raise HTTPException(status_code=401, detail="Invalid token")
    
    if reservation.date < datetime.now().date():
        raise HTTPException(status_code=400, detail="The date must be today or later.")
    
    existing_reservation = await database.get_reservation_by_date(user.id, reservation.date)
    if existing_reservation:
        raise HTTPException(status_code=400, detail="You have already created a reservation for this date.")
    
    ticket_number = await generate_ticket_number(reservation.date)
    reservation_id = await database.update_reservation(reservation_id, user.id, ticket_number, reservation.date)
    updated_reservation = await database.get_reservation_by_id(reservation_id)
    return {"message": "Reservation updated successfully", "data": updated_reservation}

@app.delete("/reservations/{reservation_id}")
async def delete_reservation(reservation_id: int, token: str = Depends(oauth2_scheme)):
    user = await authenticate_user_by_token(token)
    if not user:
        raise HTTPException(status_code=401, detail="Invalid token")
    await database.delete_reservation(reservation_id, user.id)
    return {"message": "Reservation deleted successfully"}
